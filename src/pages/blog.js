import React from "react"

import Layout from "../components/layout/layout"
import SEO from "../components/seo/seo"

const BlogPage = () => (
  <Layout>
    <SEO title="Blog" />
    <h1>Contacto</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At cumque deleniti doloribus facilis fuga, magni
      nobis tempora? Assumenda corporis debitis, ducimus, eligendi explicabo facere nihil quae quam sapiente sunt vitae!
    </p>
    <p>Aspernatur blanditiis delectus ducimus facilis ipsam laborum nesciunt provident soluta? Assumenda eaque esse
      ipsum labore maiores, numquam quas sequi veritatis? Ad autem ea enim et molestias quidem quis ratione sequi!
    </p>
  </Layout>
)

export default BlogPage
