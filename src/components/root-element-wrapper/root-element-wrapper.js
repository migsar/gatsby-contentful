import React from 'react'
import { Global, css } from '@emotion/core'

import reset from './reset.style'

// This is a funcion component that injects both store and fela for style
export default ({ element }) => (
  <>
    <Global styles={css`${reset}`} />
    {element}
  </>
);